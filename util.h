/*
This file is part of jpath.

jpath is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

jpath is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with jpath.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef UTIL_H
#define	UTIL_H

#include <vector>
#include <string>

unsigned int constexpr const_hash(char const *input) {
	return *input ?
			static_cast<unsigned int> (*input) + 33 * const_hash(input + 1) :
			5381;
}

std::vector<std::string> tokenize(const std::string& input, const std::string delim);

#endif	/* UTIL_H */