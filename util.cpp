/*
This file is part of jpath.

jpath is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

jpath is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with jpath.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.h"

void tokenize_recurse(const std::string& str, const std::string& delim, std::vector<std::string>& out) {
	auto index = str.find(delim);

	if (index != str.npos) {
		out.push_back(str.substr(0, index));
		tokenize_recurse(str.substr(index + delim.length()), delim, out);
	} else
		out.push_back(str);
}

std::vector<std::string> tokenize(const std::string& input, const std::string delim) {
	std::vector<std::string> ret;

	tokenize_recurse(input, delim, ret);

	return ret;
}