# README #

###What is it?###

* Trivially retrieve single document values from an arbitrary valid JSON document.
* Values respect traditional [JSON](http://json.org) encoding scheme.

###How to build###
To build, simply run 
```
#!bash

make
```
in the Makefile directory. Binaries are produced in bin/ folder

###How to run###

```
#!bash

$ echo '{ "key":{ "key":"value" } }' | jpath key/key
"value"
$ echo '{ "key":{ "key":"value" } }' | jpath key
{
	"key":"value"
}
$ echo '{"key":{"key":["value"] } }' | jpath key/key/0
"value"

$ jpath key/key document.json
"value"
```
Strings are always enclosed in quotes, so shell removal may be necessary.
Arrays are indexed from 0, and accessed as a path, so
```
jpath key/key/0/key/key/0
```
would read
```
{ "key" : { "key" : [ { "key" : { "key" : [ "value" ] } } ] } }
```